/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sicemal.entidades;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author usuario
 */
@Embeddable
public class ProdutoInsumoID implements java.io.Serializable {

    private Produto produto;
    private Insumo insumo;
 
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProdutoInsumoID that = (ProdutoInsumoID) o;

        if (getProduto() != null ? !produto.equals(that.produto) : that.getProduto() != null) {
            return false;
        }
        if (getInsumo() != null ? !insumo.equals(that.insumo) : that.getInsumo() != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result;
        result = (getProduto() != null ? getProduto().hashCode() : 0);
        result = 31 * result + (getInsumo() != null ? getInsumo().hashCode() : 0);
        return result;
    }

    /**
     * @return the produto
    */
    @ManyToOne
    @JoinColumn(name="id")
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the insumo
     */
    @ManyToOne
    @JoinColumn(name="id")
    public Insumo getInsumo() {
        return insumo;
    }

    /**
     * @param insumo the insumo to set
     */
    public void setInsumo(Insumo insumo) {
        this.insumo = insumo;
    }

}

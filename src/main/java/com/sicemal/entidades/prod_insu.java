/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sicemal.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "prod_insu",
uniqueConstraints={@UniqueConstraint(columnNames = {"prod_id" , "insu_id"})})
public class prod_insu implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    //private Long id;
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid",strategy = "uuid")
    private String id;    

    @Column
    private String decricao;
    
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="prod_id")
    private Prod prods;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="insu_id")
    private insu insus;


    
/*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof prod_insu)) {
            return false;
        }
        prod_insu other = (prod_insu) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sicemal.entidades.prod_insu[ id=" + getId() + " ]";
    }
*/
    
    /**
     * @return the prods
     */
    public Prod getProds() {
        return prods;
    }

    /**
     * @param prods the prods to set
     */
    public void setProds(Prod prods) {
        this.prods = prods;
    }

    /**
     * @return the insus
     */
    public insu getInsus() {
        return insus;
    }

    /**
     * @param insus the insus to set
     */
    public void setInsus(Prod insus) {
        this.setInsus(insus);
    }

    /**
     * @param insus the insus to set
     */
    public void setInsus(insu insus) {
        this.insus = insus;
    }

    /**
     * @return the decricao
     */
    public String getDecricao() {
        return decricao;
    }

    /**
     * @param decricao the decricao to set
     */
    public void setDecricao(String decricao) {
        this.decricao = decricao;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
}

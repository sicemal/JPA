/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sicemal.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author usuario
 */
@Entity
public class ProdutoInsumo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private ProdutoInsumoID pk = new ProdutoInsumoID();
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String Mensagem;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutoInsumo)) {
            return false;
        }
        ProdutoInsumo other = (ProdutoInsumo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sicemal.entidades.ProdutoInsumo[ id=" + id + " ]";
    }

    /**
     * @return the pk
     */
    public ProdutoInsumoID getPk() {
        return pk;
    }

    /**
     * @param pk the pk to set
     */
    public void setPk(ProdutoInsumoID pk) {
        this.pk = pk;
    }
    
}

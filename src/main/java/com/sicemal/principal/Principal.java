/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sicemal.principal;

import com.sicema.adaptadores.AdaptadorBanco;
import com.sicemal.entidades.Ativo;
import com.sicemal.entidades.Cliente;
import com.sicemal.entidades.Insumo;
import com.sicemal.entidades.Produto;
import com.sicemal.entidades.ProdutoInsumo;
import com.sicemal.entidades.ProdutoInsumoID;
import com.sicemal.entidades.Versao;
import com.sicemal.entidades.insu;
import com.sicemal.entidades.Prod;
import com.sicemal.entidades.prod_insu;
import java.util.ArrayList;
import java.util.HashSet;
import javax.persistence.EntityManager;

/**
 *
 * @author usuario
 */
public class Principal {
    /*
    public static void salvar(Object obj, EntityManager em)
    {
        em.getTransaction().begin();
        em.persist(obj);
        //em.merge(obj);
        em.getTransaction().commit();
                
    }
    */
    
    public static void main(String[] args) {

        
        // Funciona com mapeamento
        Prod p = new Prod();
        p.setName("Produto");
        p = AdaptadorBanco.salvar(p);

        
        insu i1 = new insu();
        i1.setName("Insumo Um");
        i1 = AdaptadorBanco.salvar(i1);

        insu i2 = new insu();
        i2.setName("Insumo Dois");
        i2 = AdaptadorBanco.salvar(i2);

        
     
        prod_insu p_i;
        p_i = new prod_insu();
        p_i.setInsus(i1);
        p_i.setProds(p);
        p_i.setDecricao("Produto com Insumos");
        p_i = AdaptadorBanco.salvar(p_i);
        
        

        prod_insu p_i1;
        p_i1 = new prod_insu();
        p_i1.setInsus(i2);
        p_i1.setProds(p);
        p_i1.setDecricao("Produto com Insumos 1");
        
        p_i1 = AdaptadorBanco.salvar(p_i1);
        
        Prod p1 = new Prod();
        p1.setId("as");
        
        p1 = AdaptadorBanco.find(Prod.class, p1.getId());
        
        prod_insu p_i2;
        p_i2 = new prod_insu();
        p_i2.setInsus(i1);
        p_i2.setProds(p1);
        p_i2.setDecricao("Produto com Insumos 111");
        p_i2 = AdaptadorBanco.salvar(p_i2);
        
        prod_insu p_i3;
        p_i3 = new prod_insu();
        p_i3.setInsus(i2);
        p_i3.setProds(p1);
        p_i3.setDecricao("Produto com Insumos 111");
        p_i3 = AdaptadorBanco.salvar(p_i3);
        
        
        

        Ativo ativo = new Ativo();
        //salvar(ativo, em);
        
        
        ArrayList versoes = new ArrayList();
        Versao vers1 = new Versao();
        vers1.setAtivo(ativo);
        Versao vers2 = new Versao();
        vers2.setAtivo(ativo);

        versoes.add(vers1);
        versoes.add(vers2);
        
        ativo.setVersoes(versoes);
        AdaptadorBanco.salvar(ativo);

        
        
        AdaptadorBanco.fechar();

        

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
Alina Henessy
 */
package com.sicema.adaptadores;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.security.auth.login.Configuration;
import org.hibernate.SessionFactory;

/**
 *
 * @author usuario
 */
public class AdaptadorBanco {

    private static AdaptadorBanco instancia;
    
    private static EntityManagerFactory factory; // = Persistence
//                .createEntityManagerFactory("musica"); 
    private static EntityManager em; // = factory.createEntityManager();
    
    private AdaptadorBanco() {
        
    }
 
    public static synchronized void fechar()
    {
        getInstance();
        em.close();
        factory.close();
    }
    
    
    public static synchronized <T> T find(Class<T> type, String id)
    {
        getInstance();
        try
        {
            em.find(type, id);
        }
        catch (Exception e) {
            em.getTransaction().rollback();
        }        
        return em.find(type, id);
    }
    
    public static synchronized <T> T salvar(Object obj)
    {
        Object result = null;
        getInstance();
        try {
            em.getTransaction().begin();
            result = em.merge(obj);
            //result = em.persist(obj);
            em.getTransaction().commit();
            
        }
        catch (Exception e) {
            em.getTransaction().rollback();
        }
        finally{
            //em.close();
        }
        /*
        em.getTransaction().begin();
        em.merge(obj);
        em.getTransaction().commit();
        */
        return (T) result;
    }    
    
    public static synchronized AdaptadorBanco getInstance()
    {
        if (instancia == null) {
            try {
                factory = Persistence.createEntityManagerFactory("musica1");
                em = factory.createEntityManager();

                instancia = new AdaptadorBanco();
            } catch (Exception e) {

            }
        }
        return instancia;
    }
    
    
 
}
